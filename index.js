const express = require('express');
const { createProxyMiddleware } = require('http-proxy-middleware');
 
const app = express();

const  { dotenv } = require('dotenv');
require('dotenv').config()


const AWS = require('aws-sdk')
const credentials = {
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
};

console.log(credentials)
AWS.config.update({
    credentials: credentials,
    region: 'ap-northeast-1',
});
const s3 = new AWS.S3({apiVersion: '2006-03-01'});

function getSignedQuery(key){
    const params = {Bucket: 'shozemi-video', Key: key, Expires: 3600};
    const url = s3.getSignedUrl('getObject', params)
    const query = url.replace(/https:\/\/.?\//i, '')
    return query;
}

app.use('/video/:key', createProxyMiddleware({
    target: 'https://shozemi-video.s3.ap-northeast-1.amazonaws.com/',
    changeOrigin: true,
    pathRewrite: (path, req) => {
        console.log(getSignedQuery(req.params.key))
        return getSignedQuery(req.params.key);
    },
}));
app.listen(3000);